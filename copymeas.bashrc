[[ "$-" != *i* ]] && return
export DOTFILESDIR="$HOME/dotfiles"
export BHDIR="$HOME/bh" # optional directory containing the bash_history file
export STORAGEDIR="/mnt/storage"
export CODEDIR="$STORAGEDIR/code"
export JDKSDIR="$STORAGEDIR/JDKs"
export DWNDIR="$STORAGEDIR/dwn"
export TMPFILES="$STORAGEDIR/tmp"

# PROJECTS_DIRS contains paths for which:
# - The bin directory is added to PATH
# - Jars in the bin directory are added to CLASSPATH
# - The .aliases file is sourced
PROJECT_DIRS=()
PROJECT_DIRS+=("/path/to/project1")
PROJECT_DIRS+=("/path/to/project2")

source "$DOTFILESDIR/bashrc"
