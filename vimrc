set nocompatible
"Enable syntax highlighting
syntax on
"Filetype detection on, and also load plugins and indention rules for this file type
filetype plugin indent on
set history=200
"Show partial command
set showcmd
"F3 toggles highlighting search
nnoremap <silent> <F3> :set hlsearch!<CR>
"Highlight match when typing search
set incsearch
"Replace tabs with spaces. Press C-v to insert a real tab.
set expandtab
"Tab is 2 wide
set tabstop=2
"Shift by 2 spaces when shifting
set shiftwidth=2
"Number of spaces that a <Tab> counts for while peforming editing operations
set softtabstop=2
"Show line number, relative to cursor position
set number rnu
"Number column is five wide
set numberwidth=5
"The numbers should be darkgrey
highlight LineNr ctermfg=darkgrey
"The number at cursor should also be darkgrey
highlight CursorLineNr ctermfg=darkgrey
"Fold settings
set foldmethod=manual
highlight FoldColumn cterm=bold
highlight Folded cterm=bold
"Easier escaping
inoremap qq <ESC>
inoremap kj <ESC>
inoremap jk <ESC>
"ctrl-s should save buffer
nmap <C-s> :w<CR>
vmap <C-s> <Esc><c-s>gv
imap <C-s> <Esc><c-s>
"F2 should save buffer if file was changed
nmap <F2> :update<CR>
"gv restores selection
vmap <F2> <Esc><F2>gv
imap <F2> <C-o><F2>
"<C-motion key> to navigate in insert mode
inoremap <C-h> <Left>
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-l> <Right>
"Emacs-like key bindings in insert mode
inoremap <C-a> <Esc>^i
inoremap <C-e> <Esc>$a
inoremap <C-f> <Right>
inoremap <C-b> <Left>
inoremap <C-d> <C-o>x
inoremap b <C-o>b
inoremap B <C-o>B
inoremap f <C-o>e<C-o><Right>
inoremap F <C-o>E<C-o><Right>
"Hidden characters to show with :set list
set listchars=eol:↲,tab:»\ ,trail:·,extends:›,precedes:‹
"Up/down 3 rows at the time
nnoremap <C-E> 3<C-E>
nnoremap <C-Y> 3<C-Y>
"Easier navigation of panes
nnoremap <C-h> <C-w><C-h>
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>
"Allow backspacing over autoindent, line breaks, and start of insert
set backspace=indent,eol,start
"Automatic smart indention
set autoindent
set smartindent
"Air-line
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buf_label_first = 0
let g:airline#extensions#tabline#tabs_label = ''
let g:airline#extensions#tabline#tab_nr_type = 2
let g:airline#extensions#tabline#buffers_label = ''
"+ and - make pane bigger and smaller respectively
if bufwinnr(1)
  map + <C-W>+
  map - <C-W>-
endif
"Enable mouse for all modes
set mouse=a
"Go to next tab
nnoremap <C-n> :bnext<CR>
"Go to previous tab
nnoremap <C-p> :bprevious<CR>
"NerdTree on/off
nnoremap d :NERDTreeToggle<CR>
"NerdTree should be 40 wide
let NERDTreeWinSize=40
"Don't add a newline at the end of file automatically
set nofixeol
"vim-plug
"https://github.com/junegunn/vim-plug
"To install plugins: :PlugInstall
"To update plugins: :PlugUpdate
call plug#begin('~/.vim/plugged') " Plugins will be installed in ~/.vim/plugged
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
call plug#end()
"Markdown preview on/off
nnoremap m :MarkdownPreviewToggle<CR>
inoremap m <ESC>:MarkdownPreviewToggle<CR>a
"Don't close the markdown preview when the buffer is hidden
let g:mkdp_auto_close=0
"Refresh markdown on save or leaving insert mode
let g:mkdp_refresh_slow=1
"Disable sync scroll and don't show filename header
let g:mkdp_preview_options={'disable_sync_scroll':1,'disable_filename':1}
"Set the title of the page to the filename
let g:mkdp_page_title = '${name}'
