///usr/bin/env [ "$JAVA_HOME" ] && JAVA_EXE="$JAVA_HOME/bin/java" || { type -p java >/dev/null 2>&1 && JAVA_EXE=$(type -p java) || { [ -x "/usr/bin/java" ] && JAVA_EXE="/usr/bin/java" || { echo "Unable to find Java"; exit 1; } } }; "$JAVA_EXE" "${JVM_OPTS[@]}" "$0" "$@"; exit $?

/**
 * proxy-server
 *
 * Copyright (C) 2024 Philippe De Neve
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import static java.lang.Integer.parseInt;
import static java.lang.Runtime.getRuntime;
import static java.lang.String.format;
import static java.util.logging.LogManager.getLogManager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HexFormat;
import java.util.logging.Logger;

class Main {

  private static final String VERSION = "0.1.0";

  public static void main(String[] args) {
    Arguments arguments = null;

    try {
      arguments = Arguments.parse(args);
    }
    catch (NumberFormatException e) {
      System.err.println("Exception while parsing number: " + e.getMessage());
      return;
    }
    catch (IllegalArgumentException e) {
      System.err.println(e.getMessage());
      Arguments.printUsage(System.err);
      return;
    }

    if (arguments.displayHelp()) {
      Arguments.printUsage(System.out);
      return;
    }

    if (arguments.displayVersion()) {
      System.out.println(VERSION);
      return;
    }

    System.out.println("proxy-server v" + VERSION);
    new ProxyServer(arguments.listenPort(), arguments.targetName(), arguments.targetPort(), arguments.key()).start();
  }

  static {
    try {
      var logFormat = "%1$tF %1$tT | %5$s%n";
      var logConfig = """
          .level=INFO
          handlers=java.util.logging.ConsoleHandler
          java.util.logging.ConsoleHandler.level=ALL
          java.util.logging.SimpleFormatter.format=%s
          """.formatted(logFormat);
      getLogManager().readConfiguration(new ByteArrayInputStream(logConfig.getBytes()));
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

}

record Arguments(int listenPort, String targetName, int targetPort, byte key, boolean displayHelp, boolean displayVersion) {

  private static final String DEFAULT_KEY = "0x2A";

  static Arguments parse(String[] args) {
    final var keyRegex = "^0x[0-9A-F]{2}$";
    final var hexFormatter = HexFormat.of().withPrefix("0x").withUpperCase();

    var listenPort = 0;
    var targetName = (String) null;
    var targetPort = 0;
    var key = hexFormatter.parseHex(DEFAULT_KEY)[0];
    var displayHelp = false;
    var displayVersion = false;

    for (int i = 0; i < args.length; i++) {
      switch (args[i]) {
        case "-k" -> {
          var keyStr = args[++i];
          if (!keyStr.matches(keyRegex)) {
            throw new IllegalArgumentException("key \"%s\" is invalid".formatted(keyStr));
          }
          key = hexFormatter.parseHex(keyStr)[0];
        }
        case "-l" -> listenPort = parseInt(args[++i]);
        case "-t" -> {
          var targetPortStr = args[++i];
          var targetPortArr = targetPortStr.split(":");
          if (targetPortArr.length != 2) {
            throw new IllegalArgumentException("target:port \"%s\" is invalid".formatted(targetPortStr));
          }
          targetName = targetPortArr[0];
          targetPort = parseInt(targetPortArr[1]);
        }
        case "-h" -> displayHelp = true;
        case "-V" -> displayVersion = true;
        default -> System.err.format("Ignoring unknown option: %s%n", args[i]);
      }
    }

    if (!displayHelp && !displayVersion && (listenPort == 0 || targetName == null || targetPort == 0)) {
      throw new IllegalArgumentException("Missing arguments");
    }

    return new Arguments(listenPort, targetName, targetPort, key, displayHelp, displayVersion);
  }

  static void printUsage(PrintStream out) {
    out.format("""
        Usage: proxy-server.java [-k <key>] -l <port> -t <target:port> | -h | -V
            -h Display help
            -k Key to use, e.g. 0x1F. Default is %s.
            -l Port to listen on
            -t Target server and port
            -V Display version
        """, DEFAULT_KEY);
  }

}

record ProxyServer(int listenPort, String targetName, int targetPort, byte key) {

  void start() {
    try {
      var serverSocket = new ServerSocket(listenPort);
      Utils.LOG.info(format("Server started on port %d. Target server is %s:%d", listenPort, targetName, targetPort));

      getRuntime().addShutdownHook(new Thread(() -> {
        try {
          serverSocket.close();
        }
        catch (IOException e) {
          Utils.LOG.fine("Exception during closing server socket: " + e.getMessage());
        }
      }));

      while (true) {
        var clientSocket = serverSocket.accept();
        try {
          var targetSocket = new Socket(targetName, targetPort);
          Thread.ofVirtual().start(new ClientConnectionHandler(clientSocket, targetSocket, key));
        }
        catch (IOException e) {
          Utils.LOG.warning("Could not connect to target server: " + e.getMessage());
          Utils.closeSocket(clientSocket);
        }
      }
    }
    catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

}

record ClientConnectionHandler(Socket clientSocket, Socket targetSocket, byte key) implements Runnable {

  private static final int BUFFER_SIZE = 128 * 1024;

  @Override
  public void run() {
    var t1 = doSocketCopyInThread(clientSocket, targetSocket);
    var t2 = doSocketCopyInThread(targetSocket, clientSocket);
    logSockets("o");
    try {
      t1.join();
      t2.join();
    }
    catch (InterruptedException e) {
    }
    logSockets("c");
  }

  private Thread doSocketCopyInThread(Socket from, Socket to) {
    return Thread.ofVirtual().start(() -> {
      try {
        copy(from.getInputStream(), to.getOutputStream());
      }
      catch (IOException e) {
        Utils.LOG.fine(format("Exception during copy %s > %s: %s", from.getPort(), to.getPort(), e.getMessage()));
      }
      finally {
        Utils.LOG.fine(format("copy %s > %s completed", from.getPort(), to.getPort()));
        Utils.closeSocket(to);
      }
    });
  }

  private void copy(InputStream from, OutputStream to) throws IOException {
    byte[] buffer = new byte[BUFFER_SIZE];
    int read;
    while ((read = from.read(buffer)) != -1) {
      for (int i = 0; i < read; i++) {
        buffer[i] ^= key;
      }
      to.write(buffer, 0, read);
    }
  }

  void logSockets(String prefix) {
    Utils.LOG.info(format("%s %s:%d <> %s:%d",
        prefix,
        clientSocket.getInetAddress().getHostAddress(),
        clientSocket.getPort(),
        targetSocket.getInetAddress().getHostAddress(),
        targetSocket.getPort()));
  }

}

interface Utils {

  final Logger LOG = Logger.getGlobal();

  static void closeSocket(Socket socket) {
    try {
      socket.close();
    }
    catch (IOException e) {
      LOG.fine(format("Exception while closing %s: %s", socket, e.getMessage()));
    }
  }
}
