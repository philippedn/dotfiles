# Setup

1. Run `gif lfs install`. This will add necessary config to `~/.gitconfig` for git lfs to work. Can be removed afterwards when importing from `gitconfig` in `dotfiles`, where this config is present.
2. Clone `dotfiles`
3. Create or clone `bh` dir (optional, for history, in separate dir in case you want to put it in version control).
4. Run install script to copy over config files. Will ask to review these files at the end. Files will be opened in vim automatically.

To download the binaries managed by git lfs after you've already cloned the repo, install git lfs as described in the previous paragraph and then issue the following commands:

```bash
git lfs fetch
git lfs checkout
```
