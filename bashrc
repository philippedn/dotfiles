if [ -z "$DOTFILESDIR" -o ! -d "$DOTFILESDIR" ]; then
  echo "DOTFILESDIR variable empty or not a directory. Aborting..."
  return
fi

append_if_missing() {
  local ADD_TO="$1"
  local TO_ADD="$2"
  local FOUND="false"
  local IFS=":"
  for PART in $ADD_TO; do
    if [ "$PART" = "$TO_ADD" ]; then
      FOUND="true"
      break
    fi
  done
  IFS=
  if [ "$FOUND" = "false" ]; then
    local RESULT="$ADD_TO:$TO_ADD"
    echo ${RESULT#:}
  else
    echo "$ADD_TO"
  fi
}

if [ -z ${CODEDIR+x} ]; then echo "Warning: CODEDIR not set"; fi
if [ -z ${STORAGEDIR+x} ]; then echo "Warning: STORAGEDIR not set"; fi
if [ -z ${JDKSDIR+x} ]; then echo "Warning: JDKSDIR not set"; fi
if [ -z ${DWNDIR+x} ]; then echo "Warning: DWNDIR not set"; fi
if [ -z ${TMPFILES+x} ]; then echo "Warning: TMPFILES not set"; fi
if [ -z ${BHDIR+x} ]; then echo "Warning: BHDIR not set"; fi

# HISTORY SETTINGS
HISTCONTROL=ignorespace
# append / clear / reload history. This way, the history of other sessions is visible.
PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"
# infinite history part 1
HISTSIZE=
# infinite history part 2
HISTFILESIZE=
[ ! -z "$BHDIR" ] && HISTFILE=$BHDIR/bash_history
# bind M-h (alt-h) to history reload. For an unknown reason this causes the
# history to be located at the first entry. To go to last history item do M->
bind -x '"\eh":"history -c; history -r; echo history file reloaded into memory"'

# This needs to be done to make it possible to rebind C-w in inputrc
stty werase undef
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
# disable suspension when pressing C-s (to enable bash forward search)
stty -ixon
# disallow overwriting files with > by default (can be forced with >|)
set -o noclobber
# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
# colors for ls
eval $(dircolors $DOTFILESDIR/dircolors)
# set INPUTRC variable to the file to be used as inputrc
export INPUTRC="$DOTFILESDIR/inputrc"

# Set PATH and CLASSPATH
for PROJECT in "${PROJECT_DIRS[@]}" "$DOTFILESDIR"; do
  PATH=$(append_if_missing "$PATH" "$PROJECT/bin")
  [ -f "$PROJECT/.aliases" ] && source "$PROJECT/.aliases"
  while IFS= read -r -d '' JAR; do
    CLASSPATH=$(append_if_missing "$CLASSPATH" "$JAR")
  done < <(find "$PROJECT/bin" -maxdepth 1 -type f -name "*.jar" -print0 2>/dev/null)
done
CLASSPATH="$(append_if_missing "$CLASSPATH" ".")"
export CLASSPATH
export PATH

# STARSHIP
which starship &>/dev/null
if [ $? = 0 ]; then
  export STARSHIP_CONFIG="$DOTFILESDIR/starship.toml"
  THE_SHELL=$(head -n1 /proc/$$/status | awk '{print $2}')
  eval "$(starship init $THE_SHELL)"
else
  echo "Starship not installed. To install, execute:"
  echo "sh -c \"\$(curl -fsSL https://starship.rs/install.sh)\""
  PS1='\[\e[0m\]\[\e[0;36m\]\h\[\e[1;34m\][\[\e[0;34m\]\w\[\e[1;34m\]]\[\e[0;34m\]$(__git_ps1) \[\e[1;34m\]\$\[\e[0m\] '
fi
